from django.contrib import admin
from .models import Post
from .models import NewNote, Notebook, Section, Page, Content, ChangeSection

admin.site.register(Post)
admin.site.register(NewNote)
admin.site.register(Notebook)
admin.site.register(Section)
admin.site.register(Page)
admin.site.register(Content)
admin.site.register(ChangeSection)
