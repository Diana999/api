# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone



class Post(models.Model):
    num = models.CharField(max_length=100, blank=True, null=True)
    published_date = models.DateTimeField(
        blank=True, null=True, default=timezone.now())

    def publish(self):
        self.published_date = timezone.now()
        self.save()


    def __unicode__(self):
        return (self.num)


class NewNote(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    published_date = models.DateTimeField(
        blank=True, null=True, default=timezone.now())
    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return str(self.name)




class Notebook(models.Model):
    name = models.TextField(max_length=100, blank=True, null=True)
    published_date = models.DateTimeField(
        blank=True, null=True, default=timezone.now())
    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return str(self.name)

class Section(models.Model):
    id_n = models.CharField(max_length=100, blank=True, null=True)
    name = models.TextField(max_length=100, blank=True, null=True)
    published_date = models.DateTimeField(
        blank=True, null=True, default=timezone.now())
    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return str(self.name)


class Page(models.Model):
    id_n = models.CharField(max_length=100, blank=True, null=True)
    name = models.TextField(max_length=100, blank=True, null=True)
    published_date = models.DateTimeField(
        blank=True, null=True, default=timezone.now())
    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return str(self.name)


class Content(models.Model):
    id_n = models.CharField(max_length=100, blank=True, null=True)
    name = models.TextField(max_length=10000, blank=True, null=True)
    published_date = models.DateTimeField(
        blank=True, null=True, default=timezone.now())
    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return str(self.name)



class ChangeSection(models.Model):
    id_n = models.CharField(max_length=100, blank=True, null=True)
    name = models.TextField(max_length=100, blank=True, null=True)
    published_date = models.DateTimeField(
        blank=True, null=True, default=timezone.now())
    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return str(self.name)

class ChangePage(models.Model):
    id_n = models.CharField(max_length=100, blank=True, null=True)
    name = models.TextField(max_length=100, blank=True, null=True)
    published_date = models.DateTimeField(
        blank=True, null=True, default=timezone.now())
    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return str(self.name)


class Cpage(models.Model):
    id_n = models.CharField(max_length=100, blank=True, null=True)
    name = models.TextField(max_length=100, blank=True, null=True)
    published_date = models.DateTimeField(
        blank=True, null=True, default=timezone.now())
    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return str(self.name)