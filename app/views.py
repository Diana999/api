# -*- coding: utf-8 -*-
import html.parser, requests
import time
from django.utils import timezone
from django.shortcuts import redirect, render, get_object_or_404
import lxml.html as html
from selenium import webdriver
from .models import Post, NewNote, Notebook, Section, Page, Content, ChangeSection, ChangePage, Cpage
from .forms import PostForm, NewNoteForm, NotebookForm, SectionForm, PageForm, ContentForm, ChangeSectionForm, ChangePageForm, CpageForm
from bs4 import BeautifulSoup

def begin_page(request):
    return render(request, 'app/begin_page.html', {})

def upload(request):
    return render(request, 'app/upload.html', {})

def change(request):
    return render(request, 'app/change.html', {})


def show_abils(request):
    return render(request, 'app/abills_page.html', {})


def start(request):
    return render(request, 'app/start.html', {})


def one_note(request):
    return render(request, 'app/one_note.html', {})


class Api():
    def __init__(self):
        self._client_id = 'c9e42a73-062e-443b-af29-7e614544ccd0'  # Выдается, когда регистрируешь свое приложение, как и секретный ключ
        self._client_secret = 'dJEOKUg1MfH43ukyHW3BC8H'
        self._scopes = 'wl.basic,wl.emails,wl.photos,wl.basic,wl.imap,' \
                       'wl.offline_access,wl.signin,office.onenote,office.onenote_update_by_app,' \
                       'office.onenote_update,Notes.ReadWrite.All, office.onenote_update'  # области,доступ на которые нужно получить, наример, здесь фото, почта, блокноты
        self._response_type = 'code'  # мы получаем в ответ на запрос код, который потом используем для получение acces+token
        self._redirect_uri = 'https://en.wikipedia.org/wiki/Norway'
        self._login_string = "https://login.live.com/oauth20_authorize.srf?client_id={self._client_id}" \
                             "&scope={self._scopes}&response_type={self._response_type}&redirect_uri={self._redirect_uri}"
        self._access_token = None
        self._data = {
            "client_id": self._client_id,
            "client_secret": self._client_secret,
            "grant_type": "authorization_code",
            "redirect_uri": self._redirect_uri,
            "code": None,
        }
        self._headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "X-Correlationld": "<GUID>",
        }

    def get_token(self):
        api = self
        driver = webdriver.Chrome('/Users/dianagajnutdinova/Downloads/chromedriver')  # переделать?
        driver.get(self._login_string.format(self=self))
        login_form = driver.find_element_by_id('i0116')
        login_form.send_keys("")
        login_form = driver.find_element_by_id('idSIButton9')
        login_form.click()
        time.sleep(0.5)
        login_form = driver.find_element_by_id('i0118')
        login_form.send_keys("")
        login_form = driver.find_element_by_id('idSIButton9')
        login_form.click()
        time.sleep(5)  # за это время человек должен успеть авторизовться на сайте, можно его изменить
        code_url = driver.current_url
        code = code_url[code_url.rfind("code=") + 5: len(code_url)]
        autorization_url = "https://login.live.com/oauth20_token.srf"
        self._data["code"] = code
        info_request = requests.post(autorization_url, headers=self._headers, data=self._data)
        response = info_request.json()
        self._access_token = (response['access_token'])
        self._headers["Authorization"] = "Bearer " + self._access_token
        self._headers["Content-Type"] = "application/json"
        driver.close()
        return self._access_token

    def user_information(self):
        url = "https://apis.live.net/v5.0/me?access_token=" + self._access_token
        user_info = requests.get(url).json()
        return (user_info)

    def notebooks(self):
        note_url = "https://www.onenote.com/api/v1.0/notebooks/?count=true"
        note_about = requests.get(note_url, headers=self._headers, data=self._data)
        notebooks_number = note_about.json()['@odata.count']
        value_n = note_about.json()["value"]
        dicn = {}
        for i in range(0, notebooks_number):
            dicn[value_n[i]["name"]] = value_n[i]["id"]
        return notebooks_number, dicn

    def sections(self, id_n):
        sections_url = "https://www.onenote.com/api/v1.0/me/notes/notebooks/" + id_n + "/sections/?count=true"
        sections_about = requests.get(sections_url, headers=self._headers, data=self._data)
        sections_number = sections_about.json()['@odata.count']
        value = sections_about.json()['value']
        dics = {}
        for i in range(0, sections_number):
            dics[value[i]["name"]] = value[i]["id"]
        return dics, sections_number

    def pages(self, id_s):
        pages_url = "https://www.onenote.com/api/v1.0/me/notes/sections/" + str(id_s) + "/pages?count=true"
        pages_about = requests.get(pages_url, headers=self._headers, data=self._data)
        if pages_about.json() == '':
            return {}, 0
        pages_number = pages_about.json()['@odata.count']
        value = pages_about.json()['value']
        dicp = {}
        for i in range(0, pages_number):
            dicp[value[i]["title"]] = value[i]["id"]
        return (dicp, pages_number)

    def deep_page(self, id_dp):
        deep_pages_url = "https://www.onenote.com/api/v1.0/me/notes/pages/" + str(id_dp) + "/content"
        deep_pages_about = requests.get(deep_pages_url, headers=self._headers, data=self._data)
        return deep_pages_about.text

    def delete_page_id(self, id_dp):
        del_url = 'https://www.onenote.com/api/v1.0/me/notes/pages/' + id_dp
        page_del = requests.delete(del_url, headers=self._headers)
        return page_del

    def data(self):
        return self._access_token


def new_get_info(request):
    api = Api()
    api.get_token()
    info = api.user_information()
    return render(request, 'app/info.html', {'info': info})


def new_notebooks(request):

    api = Api()
    api.get_token()
    nnn, notes = api.notebooks()
    form = PostForm(request.POST or None)
    if form.is_valid():
        save_it = form.save(commit=False)
        save_it.save()
        return redirect('notebook_detail')
    else:
        return render(request, 'app/notebooks.html', {'dici': notes, "n": nnn, 'form': form})#{'num': number, 'form': form, 'dici': dici})


def notebook_detail(request):
    pp = Post.objects.latest('published_date') #получили id того ноутбука, который нам нужен
    api = Api()
    for_pages = []
    pages = []
    np = []
    my_dics = {}
    md = {}
    ld = {}
    here = 0
    content = []
    api.get_token()
    n, nn = api.notebooks()
    for q in range(n):
        if list(nn.values())[q] == pp.num:
            break
    sections, ns = api.sections(pp.num)
    i = 0
    for i in range(ns):

        p, n = (api.pages(list(sections.values())[i]))

        if n == 0:
            md[0] = 'nothing'
            np.append(n)
            my_dics[list(sections.keys())[i]] = md
        else:
            for j in range(n):
                #print(((list(p.keys())[j])))
                dil = ((api.deep_page(list(p.values())[j])))
                soup = BeautifulSoup(dil, 'html.parser')
                soup.prettify()

                md[list(p.keys())[j]] = {list(p.values())[j]: (soup.get_text()).replace(((list(p.keys())[j])), '')}



            my_dics[list(sections.keys())[i]] = md
            pages.append(p)
        md = {}
    form = NewNoteForm(request.POST or None)
    words = {
        "pp": pp,
        "name": list(nn.keys())[q],
        'ns': ns,
        'pages': for_pages,
        'form': form,
        'dict': my_dics,
        'content': content,
        'ld': ld,
        'pages': pages,
    }
    if ns > 0:
        words['sections'] = sections
    if form.is_valid():
        save_it = form.save(commit=False)
        save_it.save()
        return redirect('delete_page')
    else:
        return render(request, 'app/notebooks_detail.html', words)  # {'num': number, 'form': form, 'dici': dici})

def delete_page(request):
    api = Api()
    api.get_token()

    p = NewNote.objects.latest('published_date')
    d = api.delete_page_id(p.name)
    return render(request, 'app/delete.html', {'d': d})

def new_note(request):
    form = NewNoteForm(request.POST or None)
    if form.is_valid():
        save_it = form.save(commit=False)
        save_it.save()
        return redirect('obtain_n')
    else:
        return render(request, 'app/new_note.html', {'form': form})  # {'num': number, 'form': form, 'dici': dici})


def obtain_n(request):
    p = NewNote.objects.latest('published_date')
    values = """
      {
        "name": "M"
      }
    """
    values = values.replace("M", str(p.name))
    api = Api()
    a = api.get_token()

    cr_headers = {
        "Authorization": "Bearer " + a,
        'Content-Type': 'application/json'  # это важно
    }
    url = 'HTTPS://www.onenote.com/api/v1.0/notebooks'
    u = requests.post(url, headers=cr_headers, data=values)
    print (u)
    return render(request, 'app/start.html', {'u':u})




def delete(request):
    return render(request, 'app/delete.html', {})



def new_notebook(request):
    form = NotebookForm(request.POST or None)
    if form.is_valid():
        save_it = form.save(commit=False)
        save_it.save()
        return redirect('upload_notebook')
    else:
        return render(request, 'app/new_note.html', {'form': form})  # {'num': number, 'form': form, 'dici': dici})

def upload_notebook(request):
    api = Api()
    api.get_token()
    access_token = api.data()
    headers = {
        "Authorization": "Bearer " + access_token,
        'Content-Type': 'application/json'  # это важно
    }
    url = 'https://www.onenote.com/api/v1.0/me/notes/notebooks'
    values = """
              {
                "name": "NAME"
              }
            """
    p = Notebook.objects.latest('published_date')
    values = values.replace("NAME", str(p.name))
    u = requests.post(url = url, headers=headers, data=values)
    return render(request, 'app/my.html', {'name': p.name, "type": 'Записная книга'})


def new_section(request):
    api = Api()
    api.get_token()
    n, notes = api.notebooks()
    form = SectionForm(request.POST or None)
    if form.is_valid():
        save_it = form.save(commit=False)
        save_it.save()
        return redirect('upload_section')
    else:
        return render(request, 'app/new_section.html', {'form': form, 'n':n, 'notes':notes})  # {'num': number, 'form': form, 'dici': dici})

def upload_section(request):
    api = Api()
    api.get_token()
    access_token = api.data()
    headers = {
        "Authorization": "Bearer " + access_token,
        'Content-Type': 'application/json'  # это важно
    }

    p = Section.objects.latest('published_date')
    url = 'https://www.onenote.com/api/v1.0/me/notes/notebooks/' + p.id_n + '/sectionGroups'
    values = """
                  {
                    "name": "NAME"
                  }
                """
    values = values.replace("NAME", str(p.name))
    u = requests.post(url = url, headers=headers, data=values)
    return render(request, 'app/my.html', {'name': p.name, "type": 'Секция'})


def new_section(request):
    api = Api()
    api.get_token()
    n, notes = api.notebooks()
    form = SectionForm(request.POST or None)
    if form.is_valid():
        save_it = form.save(commit=False)
        save_it.save()
        return redirect('upload_section')
    else:
        return render(request, 'app/new_section.html', {'form': form, 'n':n, 'notes':notes})  # {'num': number, 'form': form, 'dici': dici})

def upload_section(request):
    api = Api()
    api.get_token()
    access_token = api.data()
    headers = {
        "Authorization": "Bearer " + access_token,
        'Content-Type': 'application/json'  # это важно
    }

    p = Section.objects.latest('published_date')
    url = 'https://www.onenote.com/api/v1.0/me/notes/notebooks/' + p.id_n + '/sections'
    values = """
                  {
                    "name": "NAME"
                  }
                """
    values = values.replace("NAME", str(p.name))
    u = requests.post(url = url, headers=headers, data=values)
    return render(request, 'app/my.html', {'name': p.name})


def new_page(request):
    api = Api()
    api.get_token()
    d = {}
    v = {}
    api = Api()

    api.get_token()
    n, notes = api.notebooks()
    #for i in range(n):
    ##    v, s = api.sections(list(notes.values())[i])
    #    d[list(notes.keys())[i]] = v

    form = PageForm(request.POST or None)
    if form.is_valid():
        save_it = form.save(commit=False)
        save_it.save()
        return redirect('upload_page')
    else:
        return render(request, 'app/new_page.html', {'form': form, 'd': notes})  # {'num': number, 'form': form, 'dici': dici})

def upload_page(request):


    api = Api()
    api.get_token()
    access_token = api.data()
    headers = {
        "Authorization": "Bearer " + access_token,
        'Content-Type': 'application/json'  # это важно
    }

    p = Page.objects.latest('published_date')
    url = 'https://www.onenote.com/api/v1.0/me/notes/notebooks/' + p.id_n + '/sections'
    values = """
                  {
                    "name": "NAME"
                  }
                """
    values = values.replace("NAME", str(p.name))
    u = requests.post(url = url, headers=headers, data=values)
    return render(request, 'app/my.html', {'name': p.name, 'u':u })


def new_content(request):
    api = Api()
    api.get_token()
    d = {}
    v = {}
    api = Api()

    api.get_token()
    n, notes = api.notebooks()
    for i in range(n):
        v, s = api.sections(list(notes.values())[i])
        d[list(notes.keys())[i]] = v
    example = '<?xml version="1.0" encoding="utf-8" ?> \
    <html> <head> <title>Page with an absolutely positioned div and image</title> \
    </head><body data-absolute-enabled="true"><div style="position:absolute;width:280px;top:120px;left:68px"> \
    <p>New page.</p><p>The body must specify data-absolute-enabled=&quot;true&quot; and the absolutely positioned elements must specify style=&quot;position:absolute&quot;.</p> \
    <p>An absolutely positioned div.</p></div><img style="position:absolute;top:120px;left:360px;width:250px"  src="http://placecorgi.com/600" /></body></html>'
    form = ContentForm(request.POST or None)
    if form.is_valid():
        save_it = form.save(commit=False)
        save_it.save()
        return redirect('upload_content')
    else:
        return render(request, 'app/new_content.html', {'form': form, 'd': d, 'example': example})  # {'num': number, 'form': form, 'dici': dici})

def upload_content(request):


    api = Api()
    api.get_token()
    access_token = api.data()
    headers = {
        "Authorization": "Bearer " + access_token,
        'Content-Type': 'application/xhtml+xml'  # это важно
    }

    p = Content.objects.latest('published_date')
    url = 'https://www.onenote.com/api/v1.0/me/notes/sections/' + p.id_n + '/pages'
    values = str(p.name)
    u = requests.post(url = url, headers=headers, data=values)
    return render(request, 'app/my.html', {'u':u })


def change_section(request):
    api = Api()
    api.get_token()
    d = {}
    v = {}
    api = Api()

    api.get_token()
    n, notes = api.notebooks()
    for i in range(n):
        v, s = api.sections(list(notes.values())[i])
        d[list(notes.keys())[i]] = v

    form = ChangeSectionForm(request.POST or None)
    if form.is_valid():
        save_it = form.save(commit=False)
        save_it.save()
        return redirect('changes')
    else:
        return render(request, 'app/change_section.html', {'form': form, 'd': d})  # {'num': number, 'form': form, 'dici': dici})

def changes(request):

    api = Api()
    api.get_token()
    access_token = api.data()
    headers = {
        "Authorization": "Bearer " + access_token,
        'Content-Type': 'application/json'  # это важно
    }

    p = ChangeSection.objects.latest('published_date')
    url = 'https://www.onenote.com/api/v1.0/me/notes/sections/' + p.id_n
    values = """
                  {
                    "name": "NAME"
                  }
                """
    values = values.replace("NAME", str(p.name))
    u = requests.patch(url = url, headers=headers, data=values)
    return render(request, 'app/my.html', {'name': p.name, 'u':u })

def change_page(request):
    api = Api()
    api.get_token()
    d = {}
    v = {}
    api = Api()

    api.get_token()
    n, notes = api.notebooks()
    for i in range(n):
        v, s = api.sections(list(notes.values())[i])
        d[list(notes.keys())[i]] = v

    form = ChangePageForm(request.POST or None)
    if form.is_valid():
        save_it = form.save(commit=False)
        save_it.save()
        return redirect('pagec')
    else:
        return render(request, 'app/change_page.html', {'form': form, 'dici': notes})  # {'num': number, 'form': form, 'dici': dici})


def pagec(request):
    pp = ChangePage.objects.latest('published_date')  # получили id того ноутбука, который нам нужен
    api = Api()
    for_pages = []
    pages = []
    np = []
    my_dics = {}
    md = {}
    ld = {}
    here = 0
    content = []
    api.get_token()
    n, nn = api.notebooks()
    for q in range(n):
        if list(nn.values())[q] == pp.id_n:
            break
    sections, ns = api.sections(pp.id_n)
    i = 0
    for i in range(ns):

        p, n = (api.pages(list(sections.values())[i]))

        if n == 0:
            md[0] = 'nothing'
            np.append(n)
            my_dics[list(sections.keys())[i]] = md
        else:
            for j in range(n):
                # print(((list(p.keys())[j])))
                dil = ((api.deep_page(list(p.values())[j])))


                md[list(p.keys())[j]] = {list(p.values())[j]: dil}

            my_dics[list(sections.keys())[i]] = md
            pages.append(p)
        md = {}
    form = CpageForm(request.POST or None)
    example = [
        {'target': '#item','action': 'append','content': '<p>A new paragraph at the bottom of the container #item</p>'},
        {'target': '#item','action': 'append','position': 'before','content': '<h2>A subheader at the top of the container #item</h2>'}]
    words = {
        "pp": pp,
        'example': example,
        "name": list(nn.keys())[q],
        'ns': ns,
        'pages': for_pages,
        'form': form,
        'dict': my_dics,
        'content': content,
        'ld': ld,
        'pages': pages,
    }
    if ns > 0:
        words['sections'] = sections
    if form.is_valid():
        save_it = form.save(commit=False)
        save_it.save()
        return redirect('last_change_page')
    else:
        return render(request, 'app/page_choice.html', words)  # {'num': number, 'form': form, 'dici': dici})

def last_change_page(request):
    p = Cpage.objects.latest('published_date')
    url = 'https://www.onenote.com/api/v1.0/me/notes/pages/' + p.id_n + '/content'
    values = str(p.name)
    api = Api()
    api.get_token()
    access_token = api.data()
    headers = {
        "Authorization": "Bearer " + access_token,
        'Content-Type': 'application/json'  # это важно
    }
    u = requests.patch(url = url, headers=headers, data=values)
    return render(request, 'app/success.html', {'name': p.name, 'u': u})