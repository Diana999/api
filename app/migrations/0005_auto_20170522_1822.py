# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-22 15:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_auto_20170522_1820'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='author',
        ),
        migrations.RemoveField(
            model_name='post',
            name='idi',
        ),
        migrations.AddField(
            model_name='post',
            name='id',
            field=models.AutoField(auto_created=True, default=1, primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='post',
            name='number',
            field=models.IntegerField(default=1),
        ),
    ]
