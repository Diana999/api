# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-25 19:41
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0017_auto_20170525_2239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='published_date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2017, 5, 25, 19, 41, 45, 841812, tzinfo=utc), null=True),
        ),
    ]
