# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-27 00:35
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0021_auto_20170526_1103'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newnote',
            name='name',
            field=models.TextField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='newnote',
            name='published_date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2017, 5, 27, 0, 35, 33, 199127, tzinfo=utc), null=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='published_date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2017, 5, 27, 0, 35, 33, 198671, tzinfo=utc), null=True),
        ),
    ]
