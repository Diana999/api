# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-26 08:03
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0019_auto_20170525_2258'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newnote',
            name='published_date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2017, 5, 26, 8, 3, 11, 974526, tzinfo=utc), null=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='published_date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2017, 5, 26, 8, 3, 11, 974046, tzinfo=utc), null=True),
        ),
    ]
