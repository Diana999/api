from django.conf.urls import include, url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^$', views.begin_page, name='begin_page'),
    url(r'^abilities/$', views.show_abils, name='show_abils'),
    url(r'^start/$', views.start, name='start'),
    url(r'^s/$', views.obtain_n, name='obtain_n'),
    url(r'^info/$', views.new_get_info, name='new_get_info'),
    url(r'^change/$', views.change, name='change'),

    url(r'^upload/$', views.upload, name='upload'),
    url(r'^upload/notebook$', views.new_notebook, name='new_notebook'),
    url(r'^upload/notebooking$', views.upload_notebook, name='upload_notebook'),
    url(r'^upload/section$', views.new_section, name='new_section'),
    url(r'^upload/sectioning$', views.upload_section, name='upload_section'),
    url(r'^change/section$', views.change_section, name='change_section'),
    url(r'^change/sectioning$', views.changes, name='changes'),
    url(r'^change/page$', views.change_page, name='change_page'),
    url(r'^change/pageoning$', views.pagec, name='pagec'),
    url(r'^change/success', views.last_change_page, name='last_change_page'),
    url(r'^upload/content$', views.new_content, name='new_content'),
    url(r'^upload/contenting$', views.upload_content, name='upload_content'),
    url(r'^upload/page$', views.new_page, name='new_page'),
    url(r'^upload/pageing$', views.upload_page, name='upload_page'),
    url(r'^start/Onenote/$', views.one_note, name='one_note'),
    url(r'^notebooks/$', views.new_notebooks, name='new_notebooks'),
    url(r'^new_note/$', views.new_note, name='new_note'),
    url(r'^notebook_detail/$', views.notebook_detail, name='notebook_detail'),
    url(r'^delete_page/$', views.delete_page, name='delete_page'),
]
