from django import forms
from .models import Post, Section, Page, Content, ChangeSection, ChangePage, Cpage
from .models import NewNote, Notebook
class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('num','published_date',)


class NewNoteForm(forms.ModelForm):
    class Meta:
        model = NewNote
        fields = ('name','published_date',)


class NotebookForm(forms.ModelForm):
    class Meta:
        model = Notebook
        fields = ('name','published_date',)

class SectionForm(forms.ModelForm):
    class Meta:
        model = Section
        fields = ('name','published_date','id_n', )

class PageForm(forms.ModelForm):
    class Meta:
        model = Page
        fields = ('name','published_date','id_n', )

class ContentForm(forms.ModelForm):
    class Meta:
        model = Content
        fields = ('name','published_date','id_n', )


class ChangeSectionForm(forms.ModelForm):
    class Meta:
        model = ChangeSection
        fields = ('name','published_date','id_n', )


class ChangePageForm(forms.ModelForm):
    class Meta:
        model = ChangePage
        fields = ('name','published_date','id_n', )


class CpageForm(forms.ModelForm):
    class Meta:
        model = Cpage
        fields = ('name','published_date','id_n', )
